﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxManipulator : MonoBehaviour
{

    public Light dirLight;
    public ParticleSystem ps;
    public Light dirLight2;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        dirLight.range += Random.Range(-80, 80);
        dirLight.range = Mathf.Clamp(dirLight.range, -80   , 80);

        dirLight2.transform.Rotate(new Vector3(0, dirLight2.transform.rotation.y + 5, 0));
        ps.transform.Rotate(0, 0, ps.transform.rotation.z + Random.Range(0, 2));
    }
}
